## Web app chatbot

* default url : localhost:5000

## Create Telegram Bot
The first thing you need to do is tell Telegram that you want to create a bot. For this, you’ll need a Telegram account – install their app on your phone, and get it set up.

Next, start a conversation with the “BotFather”. This is a bot that Telegram themselves run, and it controls the creation and registration of bots on their platform. On the Android version of their app, here’s what you do (other platforms are similar)

* To start a new chat, tap the start conversation button in the bottom right corner.
* Tap the magnifying glass “Search” icon near the top right.
* Type “botfather”. or click on @botfather
* Tap on the “@BotFather” that appears. Make sure it has a blue checkmark next to it.
* It will show a greeting message.
* Click the “Start” button.
* Send it a message “/newbot”
* It will prompt you to give your bot a name.
* It will then ask for a username.
* If everything goes well, it will print a message stating that the bot has been created. There's one crucial piece of information in there that you'll need later: the HTTP API access token. It'll be a long string of alphanumeric characters, possibly including a colon. copied these token and place on config.json under telegram_api key and set telegram_on to true if used telegram api.
* bot name : testbotapi1 ,  username : hyperAD4_bot