const msgerForm = get(".msger-inputarea");
const msgerInput = get(".msger-input");
const msgerChat = get(".msger-chat");
var timeoutmessage = 300
const key = 'adkjgkahfiyoHAKDKABk'
var inactivecount = 0
var post = false
var socket = io.connect('http://' + document.domain + ':' + location.port);

// Icons made by Freepik from www.flaticon.com
const BOT_IMG = "https://image.flaticon.com/icons/svg/327/327779.svg";
const PERSON_IMG = "https://image.flaticon.com/icons/svg/145/145867.svg";
const BOT_NAME = "Bot";
const PERSON_NAME = "Me";

msgerForm.addEventListener("submit", event => {
    event.preventDefault();

    const msgText = msgerInput.value;
    if (!msgText) return;

    socket.emit('question', {
        data: msgText,
        key: key
    });

    appendMessage(PERSON_NAME, PERSON_IMG, "right", msgText, true);
    msgerInput.value = "";
});

socket.on('response', function(msg) {
    const msgText = msg.data;
    const delay = msgText.split(" ").length * 100;

    setTimeout(() => {
        appendMessage(BOT_NAME, BOT_IMG, "left", msgText);
    }, delay);
});

socket.on('opening', function(msg) {
    const msgText = msg.data;
    const delay = msgText.split(" ").length * 100;
    timeoutmessage = msg.idle_seconds;

    setTimeout(() => {
        appendMessage(BOT_NAME, BOT_IMG, "left", msgText);
    }, delay);
});

socket.on('idle', function(msg) {
    const msgText = msg.data;
    const delay = msgText.split(" ").length * 100;

    setTimeout(() => {
        appendMessage(BOT_NAME, BOT_IMG, "left", msgText);
    }, delay);
});

function appendMessage(name, img, side, text, append = false) {
    //   Simple solution for small apps
    const msgHTML = `
        <div class="msg ${side}-msg">
        <div class="msg-img" style="background-image: url(${img})"></div>

        <div class="msg-bubble">
            <div class="msg-info">
            <div class="msg-info-name">${name}</div>
            <div class="msg-info-time">${formatDate(new Date())}</div>
            </div>

            <div class="msg-text">${text}</div>
        </div>
        </div>
    `;
    if (append) {
        inactivecount = timeoutmessage;
        post = true
    }

    msgerChat.insertAdjacentHTML("beforeend", msgHTML);
    msgerChat.scrollTop += 500;
}

// Utils
function get(selector, root = document) {
    return root.querySelector(selector);
}

function formatDate(date) {
    const h = "0" + date.getHours();
    const m = "0" + date.getMinutes();

    return `${h.slice(-2)}:${m.slice(-2)}`;
}

function random(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function reduceInterval(num) {
    inactivecount = num - 1
}

var x = setInterval(function() {
    if (post == true && inactivecount == 0) {
        socket.emit('idle', {
            key: key
        });
        post = false
    } else if (inactivecount >= 0) {
        inactivecount = inactivecount - 1
    }
}, 1000);