FROM ubuntu:latest

MAINTAINER Achmad Andi Badra

CMD tail -f /dev/null

RUN apt-get update -y && apt-get install -y python3-pip python-dev

EXPOSE 80
EXPOSE 5000

COPY ./requirements.txt /chatbotapi/requirements.txt

WORKDIR /chatbotapi

RUN pip3 install -r requirements.txt

COPY . /chatbotapi

ENTRYPOINT ["python3"]
CMD["run.py"]