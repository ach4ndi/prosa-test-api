from flask import render_template, request, session, jsonify
from flask_socketio import emit
from chat_bot.main import app, socketio

from telegram.ext import *

import json, io

key = 'adkjgkahfiyoHAKDKABk'

f = open("question.json", "r")
answers = json.loads(f.read())
f.close()

f = open("default.json", "r")
message_default = json.loads(f.read())
f.close()

f = open("config.json", "r")
config_load = json.loads(f.read())

key = config_load['key']
TELEGRAM_API = config_load['telegram_api']
TELEGRAM_ON = config_load['telegram_on']

f.close()

def start(update, context):
    update.message.reply_text(message_default['opening'])

def handle_message(update, context):
    text = str(update.message.text).lower()
    
    answer = answers.get(text, message_default['not_match_word'])
    #logging.info(f'User ({update.message.chat.id}) says: {text}')

    # Bot response
    #response = responses.get_response(text)
    update.message.reply_text(answer)

try:
    if TELEGRAM_ON:
        updater = Updater(TELEGRAM_API, use_context=True)
        dp = updater.dispatcher
        dp.add_handler(CommandHandler('start', start))
        dp.add_handler(MessageHandler(Filters.text, handle_message))
        
        # Run the bot
        updater.start_polling(1.0)
        # Idle state give bot time to go in idle
        updater.idle()
except Exception as ex:
    print('telegram error'+str(ex))

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/api/login', methods=['POST'])
def login():
    username = request.form.get('username')
    return jsonify({'username': username})

@socketio.event
def connect():
    emit('opening', {'data': message_default['opening'],'idle_seconds':config_load['set_idle_period']})

@socketio.on('question')
def send_response(question):
    if key == question['key']:
        answer = answers.get(question['data'].lower(), message_default['not_match_word'])
        emit('response', {'data': answer})

@socketio.on('idle')
def send_response(idle):
    if key == idle['key']:
        emit('idle', {'data': message_default['idle']})